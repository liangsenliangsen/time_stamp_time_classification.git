//
//  main.m
//  时间戳转化为时间
//
//  Created by chenleping on 2018/6/1.
//  Copyright © 2018年 IAPTest. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
