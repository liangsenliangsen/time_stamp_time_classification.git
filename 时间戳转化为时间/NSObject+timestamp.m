//
//  NSObject+timestamp.m
//  时间戳转化为时间
//
//  Created by chenleping on 2018/6/1.
//  Copyright © 2018年 IAPTest. All rights reserved.
//

#import "NSObject+timestamp.h"

@implementation NSObject (timestamp)

#pragma mark ---- 将时间戳转换成时间
- (NSString *)getTimeFromTimestamp:(double)time withDateFormat:(NSString *)timeFormat{
    // 传递进来的Time是一个秒
    NSDate * myDate=[NSDate dateWithTimeIntervalSince1970:time];
    //设置时间格式(@"YYYY-MM-dd HH:mm:ss")
    NSDateFormatter * formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:timeFormat];
    //将时间转换为字符串
    NSString *timeStr=[formatter stringFromDate:myDate];
    return timeStr;
}


@end
