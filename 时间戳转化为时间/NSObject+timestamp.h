//
//  NSObject+timestamp.h
//  时间戳转化为时间
//
//  Created by chenleping on 2018/6/1.
//  Copyright © 2018年 IAPTest. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (timestamp)

- (NSString *)getTimeFromTimestamp:(double)time withDateFormat:(NSString *)timeFormat;

@end
